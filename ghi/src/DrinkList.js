import { useGetDrinkListQuery } from './rtk/apis/drinksApi';

function DrinkList() {
  const { data, isLoading } = useGetDrinkListQuery();

  return (
    <div style={{ padding: '5px', margin: '5px auto', width: '70%' }}>
        {isLoading ? (
            <>Loading...</>
        ) : (
            <>
                {data.map(drink => (
                    <div key={drink.id} style={{ border: '1px solid black', borderRadius: '5px', padding: '5px', margin: '5px' }}>
                        <h3>{drink.name}</h3>
                        <p>({drink.type})</p>
                        <p>{drink.description}</p>
                        <p>${drink.price}</p>
                    </div>
                ))}
            </>
        )}
    </div>
  );
}

export default DrinkList;
