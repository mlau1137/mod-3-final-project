import { Provider } from 'react-redux'

import { store } from './rtk/store'
import NavContainer from './NavContainer.js'

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <NavContainer />
    </Provider>
  );
}

export default App;
