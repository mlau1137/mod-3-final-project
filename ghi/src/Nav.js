import { pages } from './constants';

function Nav({ currentPage, onPageChange }) {
  return (
    <div style={{ display: 'flex', flexDirection: 'row', columnGap: '20px', width: '100%', justifyContent: 'center', marginTop: '10px' }}>
      {pages.map(page => (
        <button
            key={page}
            style={{ borderBottom: page === currentPage ? '1px solid black' : null }}
            onClick={() => onPageChange(page)}
        >
            {page}
        </button>
      ))}
    </div>
  );
}

export default Nav;
