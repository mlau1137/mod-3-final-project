import { useSelector, useDispatch } from 'react-redux'

// state
import { setPage } from './rtk/slices/uiSlice';

// components
import DrinkForm from './DrinkForm.js'
import DrinkList from './DrinkList.js'
import Nav from './Nav.js'

import { pages } from './constants';

function NavContainer() {
  const page = useSelector((state) => state.ui.page);
  const dispatch = useDispatch();

  return (
    <>
      <Nav currentPage={page} onPageChange={(newPage) => dispatch(setPage(newPage))} />
      {page === pages[0] && (
        <DrinkList />
      )}
      {page === pages[1] && (
        <DrinkForm />
      )}
    </>
  );
}

export default NavContainer;
