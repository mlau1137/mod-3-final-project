import { useDispatch, useSelector } from 'react-redux';

import { setCreateDrinkFormData } from './rtk/slices/uiSlice';
import { useAddDrinkMutation } from './rtk/apis/drinksApi';

const inputStyles = {
  margin: '5px',
};

function DrinkForm() {
  const formData = useSelector((state) => state.ui.createDrinkFormData);
  const [addDrink, { isLoading }] = useAddDrinkMutation()
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (Object.keys(formData).length !== 4) { return; }

    addDrink(formData);
  }

  const handleChange = async (e) => {
    const { name, value } = e.target;
    dispatch(setCreateDrinkFormData({ name, value }));
  }

  return (
    <div style={{ padding: '5px', margin: '5px auto', width: '70%' }}>
      <h1>create new item</h1>
      <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={inputStyles}>
          <label>name: </label>
          <input type="text" name="name" onChange={handleChange} />
        </div>
        <div style={inputStyles}>
          <label>price: </label>
          <input type="text" name="price" onChange={handleChange} />
        </div>
        <div style={inputStyles}>
          <label>description: </label>
          <input type="text" name="description" onChange={handleChange} />
        </div>
        <div style={inputStyles}>
          <label>type: </label>
          <input type="text" name="type" onChange={handleChange} />
        </div>
        <button style={{ width: '10%', marginLeft: '10%' }} type="submit">submit</button>
      </form>
    </div>
  );
}

export default DrinkForm;
