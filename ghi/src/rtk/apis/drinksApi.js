import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const tags = {
  Drinks: 'Drinks',
}

export const drinksApi = createApi({
  reducerPath: 'drinksApi',
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.REACT_APP_API_HOST}/api` }),
  endpoints: (builder) => ({
    getDrinkList: builder.query({
      query: () => '/drinks',
      providesTags: [tags.Drinks],
    }),
    addDrink: builder.mutation({
      query(body) {
        return {
          url: '/drinks',
          method: 'POST',
          body,
        }
      },
      invalidatesTags: [tags.Drinks],
    }),
  }),
});

export const { useGetDrinkListQuery, useAddDrinkMutation } = drinksApi;
