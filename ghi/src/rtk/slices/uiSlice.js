import { createSlice } from '@reduxjs/toolkit';

import { pages } from '../../constants';

const initialState = {
    page: pages[0],
    createDrinkFormData: {},
};

export const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    setPage: (state, action) => {
        state.page = action.payload;
    },
    setCreateDrinkFormData: (state, action) => {
        state.createDrinkFormData = {
          ...state.createDrinkFormData,
          [action.payload.name]: action.payload.value
        };
    },
  },
});

export const { setPage, setCreateDrinkFormData } = uiSlice.actions;

export default uiSlice.reducer;
