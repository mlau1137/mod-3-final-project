import { configureStore } from '@reduxjs/toolkit';

import uiReducer from './slices/uiSlice';
import { drinksApi } from './apis/drinksApi';

export const store = configureStore({
  reducer: {
    ui: uiReducer,
    [drinksApi.reducerPath]: drinksApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(drinksApi.middleware),
});
