from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from pydantic import BaseModel


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "year": 2022,
            "month": 10,
            "day": "28",
            "hour": 19,
            "min": 0,
            "tz:": "PST"
        }
    }

drinks = [
    { 'id': 1, 'name': 'constant comment', 'price': 4, 'description': 'the best one', 'type': 'tea'},
    { 'id': 2, 'name': 'early grey', 'price': 4, 'description': 'also good', 'type': 'tea'},
    { 'id': 3, 'name': 'english breakfast', 'price': 4, 'description': 'the standard', 'type': 'tea'},
    { 'id': 4, 'name': 'jasmine', 'price': 4, 'description': 'yummy', 'type': 'tea'},
    { 'id': 5, 'name': 'coffee', 'price': 6, 'description': 'idk coffee, it\'s all the same to me', 'type': 'yowza caffeine'},
]

class DrinkIn(BaseModel):
    description: str
    name: str
    price: int | str
    type: str

@app.post("/api/drinks")
def create_drink(drink: DrinkIn):
    new_id = drinks[len(drinks) - 1]['id'] + 1
    drinks.append({
        'id': new_id,
        'name': drink.name,
        'price': drink.price,
        'description': drink.description,
        'type': drink.type,
    })
    return drinks

@app.get("/api/drinks")
def get_drinks():
    return drinks

@app.get("/api/drinks/{id}")
def get_drink(id: int):
    return [drink for drink in drinks if drink.id == id][0]